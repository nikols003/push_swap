/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_4.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdubina <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/10 17:47:19 by mdubina           #+#    #+#             */
/*   Updated: 2017/02/11 16:47:27 by mdubina          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	ft_sort_4(t_s *s, int pushed)
{
	if (ft_check_list(&s->a) && !pushed)
		return ;
	if (ft_lst_count(s->b) > 1)
	{
		if (*(int *)s->a->content > *(int *)s->a->next->content && \
				*(int *)s->b->content < *(int *)s->b->next->content)
			ft_transformer("ss", s);
	}
	if (*(int *)s->a->content > *(int *)s->a->next->content)
		ft_transformer("sa", s);
	if (ft_check_list(&s->a) && !pushed)
		return ;
	if (ft_check_list(&s->a) && pushed)
	{
		ft_transformer("pa", s);
		pushed--;
	}
	else
	{
		ft_transformer("pb", s);
		pushed++;
	}
	ft_sort_4(s, pushed);
}

void	ft_sort_3_more(t_s *s)
{
	if (ft_lst_count(s->b) > 1)
	{
		if (*(int *)s->a->content > *(int *)s->a->next->content && \
				*(int *)s->b->content < *(int *)s->b->next->content)
			ft_transformer("ss", s);
	}
	if (*(int *)s->a->content > *(int *)s->a->next->content)
		ft_transformer("sa", s);
	if (*(int *)s->a->next->content > \
			*(int *)s->a->next->next->content)
	{
		if (ft_lst_count(s->b) > 2 && *(int *)s->b->next->content < \
				*(int *)s->b->next->next->content)
		{
			ft_transformer("rr", s);
			ft_transformer("ss", s);
			ft_transformer("rrr", s);
		}
		else
		{
			ft_transformer("pb", s);
			ft_transformer("sa", s);
			ft_transformer("pa", s);
		}
	}
}

int		ft_lst_count(t_list *begin)
{
	int		i;
	t_list	*tmp;

	if (begin == NULL)
		return (0);
	i = 0;
	tmp = begin;
	while (tmp != NULL)
	{
		i++;
		tmp = tmp->next;
	}
	return (i);
}

int		*ft_sort(int *array, int count)
{
	int	i;
	int	max;
	int j;

	i = 0;
	while (i < count)
	{
		j = 1;
		while (j < count)
		{
			if (array[j] < array[j - 1])
			{
				max = array[j];
				array[j] = array[j - 1];
				array[j - 1] = max;
			}
			j++;
		}
		i++;
	}
	return (array);
}

void	ft_lstdelete(t_list **head)
{
	t_list *tmp;

	while (*head)
	{
		free((*head)->content);
		tmp = *head;
		*head = (*head)->next;
		free(tmp);
	}
}
