/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdubina <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/10 12:18:44 by mdubina           #+#    #+#             */
/*   Updated: 2017/02/11 16:48:06 by mdubina          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	ft_show_stack_2(t_list *head_a, t_list *head_b)
{
	ft_printf("\x1B[34m%11s\x1B[0m", "STACK A");
	ft_printf("\x1B[35m%11s\x1B[0m\n", "STACK B");
	while (head_a || head_b)
	{
		if (head_a)
		{
			ft_printf("\x1B[34m%11d\x1B[0m", *((int *)head_a->content));
			head_a = head_a->next;
		}
		else
			ft_printf("%11c", ' ');
		if (head_b)
		{
			ft_printf("\x1B[35m%11d\x1B[0m\n", *((int *)head_b->content));
			head_b = head_b->next;
		}
		else
			ft_printf("%11 \n");
	}
}

void	ft_show_stack_3(t_list *head_a, t_list *head_b)
{
	ft_printf("%11s", "STACK A");
	ft_printf("%11s\n", "STACK B");
	while (head_a || head_b)
	{
		if (head_a)
		{
			ft_printf("%11d", *((int *)head_a->content));
			head_a = head_a->next;
		}
		else
			ft_printf("%11c", ' ');
		if (head_b)
		{
			ft_printf("%11d\n", *((int *)head_b->content));
			head_b = head_b->next;
		}
		else
			ft_printf("%11 \n");
	}
}

int		main(int argc, char **argv)
{
	long int	d;
	t_s			s[1];
	t_list		*p;
	int			i;

	if (argc < 2)
		return (ft_printf("usage: ./checker [-v][-c] \"n1\" \"n2\" ...\n"));
	if (argc > 1)
		i = ft_check_flag(argv[1], argv[2], s, argc);
	while (argc > i)
	{
		if (ft_isdigit(argv[argc - 1][0]) == 0 && argv[argc - 1][0] != '-')
			return (ft_printf("Error\n"));
		d = ft_atoi(argv[argc - 1]);
		p = ft_lstnew(&d, sizeof(int));
		if (ft_validate(&s->a, d))
			return (ft_printf("Error\n"));
		ft_lstadd(&s->a, p);
		argc--;
	}
	if (s->a)
		ft_perexod(s);
	ft_lstdelete(&s->a);
	return (0);
}
