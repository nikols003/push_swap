NAME = push_swap

GCC = gcc -Wall -Wextra -Werror

FRAMEWORKS = -framework OpenGL -framework AppKit

SRC =	push_swap.c \
		ft_function.c \
		ft_operation.c \
		ft_sort_4.c \
		ft_sort_small.c \
		help1.c

OBJ = $(SRC:.c=.o)

all: $(NAME)

$(NAME): $(OBJ)
		gcc -Wall -Wextra -Werror -c $(SRC)
		cd libft/ && $(MAKE)
		cd libft/ft_printf/ && $(MAKE)
		gcc -o $(NAME) $(OBJ) libft/ft_printf/libftprintf.a libft/libft.a
		gcc -o checker ft_operation.c ft_function.c ft_sort_4.c checker.c \
		checker_help.c libft/ft_printf/libftprintf.a libft/libft.a

%.o : %.c
	$(GCC) -c -o  $@ $<

clean:
		rm -f $(OBJ)
		make clean -C libft/
		make clean -C libft/ft_printf/

fclean: clean
		make fclean -C libft/
		make fclean -C libft/ft_printf/
		rm -f $(NAME)
		rm -f checker

re: fclean all
