/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdubina <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/10 17:42:15 by mdubina           #+#    #+#             */
/*   Updated: 2017/02/11 16:56:15 by mdubina          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

# include "libft/libft.h"
# include "libft/ft_printf/libftprintf.h"
# include <time.h>
# include <stdlib.h>

typedef struct	s_stack
{
	t_list			*a;
	t_list			*b;
	int				count;
	int				l;
	int				v;
	int				c;
}				t_s;

void			rotate(t_list **head);
void			sa(t_list **head_a);
void			push(t_list **head_a, t_list **head_b);
void			r_rotate(t_list **head);
int				ft_check(t_list **head_a, t_list **head_b);
int				ft_check_list(t_list **head_a);
int				ft_check_nb(t_list **head_a, int nb);
int				ft_lst_count(t_list *begin);
void			ft_show_stack(t_list *head_a, t_list *head_b);
int				ft_avarage(t_s *s);
int				ft_avarage_2(t_s *s, int k);
int				ft_push_b_while(t_s *s);
int				ft_push_a(t_s *s, int k);
void			ft_sort_small(t_s *s);
void			ft_sort_4(t_s *s, int pushed);
int				ft_push_a_while(t_s *s, int k);
void			ft_transformer(char *line, t_s *s);
int				ft_validate(t_list **head_a, long int d);
int				ft_perexod(t_s *s);
void			ft_show_stack_2(t_list *head_a, t_list *head_b);
int				ft_check_flag(char *s1, char *s2, t_s *s, int a);
void			ft_show_stack_3(t_list *head_a, t_list *head_b);
void			ft_algoritm(t_s *s);
void			ft_sort_2(t_s *s);
void			ft_sort_3_more(t_s *s);
int				ft_check_nb_2(t_list **head_b, int nb);
int				*ft_sort(int *array, int count);
void			ft_lstdelete(t_list **head);

#endif
