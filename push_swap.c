/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdubina <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/10 16:46:47 by mdubina           #+#    #+#             */
/*   Updated: 2017/02/11 16:14:53 by mdubina          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	ft_kernel(t_s *s)
{
	if (ft_lst_count(s->a) - s->count == 4)
		ft_sort_4(s, 0);
	if (ft_lst_count(s->a) == 3 && !s->count)
		ft_sort_small(s);
	if (ft_lst_count(s->a) - s->count == 2)
		ft_sort_2(s);
	if (ft_lst_count(s->a) - s->count == 3)
	{
		ft_sort_3_more(s);
		if (ft_lst_count(s->a) > 1)
		{
			if (*(int *)(s->a)->content > *(int *)(s->b)->next->content \
				&& *(int *)(s->b)->content < \
				*(int *)(s->b)->next->content)
				ft_transformer("ss", s);
		}
		if (*(int *)(s->a)->content > *(int *)(s->a)->next->content)
			ft_transformer("sa", s);
	}
}

void	ft_algoritm_2(t_s *s)
{
	int k;

	k = ft_push_b_while(s);
	ft_algoritm(s);
	while (k)
	{
		if (k < 5)
			k = ft_push_a(s, k);
		else
		{
			k = ft_push_a_while(s, k);
			ft_algoritm(s);
		}
	}
}

void	ft_algoritm(t_s *s)
{
	if (ft_check(&s->a, &s->b))
		return ;
	if (ft_check_list(&s->a))
	{
		s->count = ft_lst_count(s->a);
		return ;
	}
	if (ft_lst_count(s->a) - s->count > 4)
		ft_algoritm_2(s);
	else
		ft_kernel(s);
	if (ft_check_list(&s->a))
	{
		s->count = ft_lst_count(s->a);
		return ;
	}
	ft_algoritm(s);
}

int		main(int argc, char **argv)
{
	long int	d;
	t_list		*p;
	t_s			s[1];

	s->a = NULL;
	p = NULL;
	s->b = NULL;
	if (argc < 2)
		return (0);
	s->count = 0;
	s->l = 0;
	while (argc > 1)
	{
		d = ft_atoi(argv[argc - 1]);
		p = ft_lstnew(&d, sizeof(int));
		if (d > 2147483647 || d < -2147483648)
			return (ft_printf("Error\n"));
		ft_lstadd(&s->a, p);
		argc--;
	}
	if (ft_lst_count(s->a) < 4)
		ft_sort_small(s);
	else
		ft_algoritm(s);
	ft_lstdelete(&s->a);
}
