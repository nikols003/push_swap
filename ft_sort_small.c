/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_small.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdubina <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/10 17:15:17 by mdubina           #+#    #+#             */
/*   Updated: 2017/02/11 16:14:01 by mdubina          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	ft_sort_3(t_s *s)
{
	if (*(int *)s->a->content < *(int *)s->a->next->content && \
			*(int *)s->a->content < *(int *)s->a->next->next->content)
	{
		ft_transformer("rra", s);
		ft_transformer("sa", s);
		return ;
	}
	if (*(int *)s->a->content > *(int *)s->a->next->content && \
			*(int *)s->a->content < *(int *)s->a->next->next->content)
	{
		ft_transformer("sa", s);
		return ;
	}
	if (*(int *)s->a->content < *(int *)s->a->next->content && \
			*(int *)s->a->content > *(int *)s->a->next->next->content)
	{
		ft_transformer("rra", s);
		return ;
	}
}

void	ft_sort_2(t_s *s)
{
	if (ft_lst_count(s->a) > 1 && ft_lst_count(s->b) > 1)
	{
		if (*(int *)s->a->content > *(int *)s->a->next->content && \
				*(int *)s->b->content < *(int *)s->b->next->content)
			ft_transformer("ss", s);
	}
	if (*(int *)s->a->content > *(int *)s->a->next->content)
		ft_transformer("sa", s);
	return ;
}

void	ft_sort_small(t_s *s)
{
	if (ft_check(&s->a, &s->b))
		return ;
	if (ft_lst_count(s->a) == 3)
	{
		ft_sort_3(s);
		if (*(int *)s->a->content > *(int *)s->a->next->content && \
				*(int *)s->a->content > *(int *)s->a->next->next->content \
				&& *(int *)s->a->next->content < \
			*(int *)s->a->next->next->content)
		{
			ft_transformer("ra", s);
			return ;
		}
		if (*(int *)s->a->content > *(int *)s->a->next->content && \
				*(int *)s->a->next->content > \
			*(int *)s->a->next->next->content)
		{
			ft_transformer("ra", s);
			ft_transformer("sa", s);
			return ;
		}
	}
	else
		ft_sort_2(s);
}

int		ft_avarage(t_s *s)
{
	t_list	*tmp;
	int		count;
	int		i;
	int		*array;

	tmp = s->a;
	count = ft_lst_count(s->a) - s->count;
	array = (int *)malloc(sizeof(int) * count);
	i = 0;
	while (i < count)
	{
		array[i] = *((int *)tmp->content);
		tmp = tmp->next;
		i++;
	}
	array = ft_sort(array, count);
	free(array);
	return (array[count / 2]);
}

int		ft_avarage_2(t_s *s, int k)
{
	t_list	*tmp;
	int		i;
	int		*array;

	tmp = s->b;
	array = (int *)malloc(sizeof(int) * k);
	i = 0;
	while (i < k)
	{
		array[i] = *((int *)tmp->content);
		tmp = tmp->next;
		i++;
	}
	array = ft_sort(array, k);
	free(array);
	return (array[k / 2]);
}
