/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_operation.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdubina <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/10 17:18:31 by mdubina           #+#    #+#             */
/*   Updated: 2017/02/10 17:18:48 by mdubina          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	sa(t_list **head_a)
{
	t_list *tmp1;

	if (*head_a && (*head_a)->next)
	{
		tmp1 = (*head_a)->next;
		(*head_a)->next = (*head_a)->next->next;
		tmp1->next = *head_a;
		*head_a = tmp1;
	}
}

void	push(t_list **head_a, t_list **head_b)
{
	t_list *tmp;

	if (*head_a == NULL)
		return ;
	tmp = *head_a;
	*head_a = (*head_a)->next;
	ft_lstadd(head_b, tmp);
}

void	rotate(t_list **head)
{
	t_list *tmp1;
	t_list *tmp2;

	if (*head == NULL)
		return ;
	if ((*head)->next == NULL)
		return ;
	tmp1 = *head;
	*head = (*head)->next;
	tmp2 = *head;
	while (tmp2->next)
		tmp2 = tmp2->next;
	tmp2->next = tmp1;
	tmp1->next = NULL;
}

void	r_rotate(t_list **head)
{
	t_list *tmp1;
	t_list *tmp2;

	if (*head == NULL)
		return ;
	if ((*head)->next == NULL)
		return ;
	tmp1 = *head;
	if (tmp1->next->next == NULL)
	{
		sa(head);
		return ;
	}
	while (tmp1->next->next)
		tmp1 = tmp1->next;
	tmp2 = tmp1->next;
	tmp1->next = NULL;
	tmp2->next = *head;
	*head = tmp2;
}

int		ft_check(t_list **head_a, t_list **head_b)
{
	t_list *tmp;

	if (*head_b != NULL)
		return (0);
	if (*head_a == NULL)
		return (0);
	if ((*head_a)->next == NULL)
		return (1);
	tmp = *head_a;
	while (tmp->next)
	{
		if (*((int *)tmp->content) > *((int *)tmp->next->content))
			return (0);
		tmp = tmp->next;
	}
	return (1);
}
