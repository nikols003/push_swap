/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker_help.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdubina <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/10 10:59:36 by mdubina           #+#    #+#             */
/*   Updated: 2017/02/11 17:14:34 by mdubina          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int		ft_check_flag(char *s1, char *s2, t_s *s, int a)
{
	s->v = 0;
	s->c = 0;
	s->b = NULL;
	s->a = NULL;
	if (s1[0] == '-')
	{
		if (s1[1] == 'v')
			s->v++;
		if (s1[1] == 'c')
			s->c++;
	}
	if (a > 2 && s2[0] == '-')
	{
		if (s2[1] == 'v')
			s->v++;
		if (s2[1] == 'c')
			s->c++;
	}
	return (s->c + s->v + 1);
}

void	ft_help(char *line, t_list **head_a, t_list **head_b)
{
	if (ft_strcmp(line, "ra") == 0)
		rotate(head_a);
	else if (ft_strcmp(line, "rb") == 0)
		rotate(head_b);
	else if (ft_strcmp(line, "rr") == 0)
	{
		rotate(head_a);
		rotate(head_b);
	}
	else if (ft_strcmp(line, "rra") == 0)
		r_rotate(head_a);
	else if (ft_strcmp(line, "rrb") == 0)
		r_rotate(head_b);
	else if (ft_strcmp(line, "rrr") == 0)
	{
		r_rotate(head_a);
		r_rotate(head_b);
	}
	else
	{
		ft_printf("Error\n");
		exit(0);
	}
}

void	ft_help_1(char *line, t_list **head_a, t_list **head_b)
{
	if (ft_strcmp(line, "sa") == 0)
		sa(head_a);
	else if (ft_strcmp(line, "sb") == 0)
		sa(head_b);
	else if (ft_strcmp(line, "ss") == 0)
	{
		sa(head_a);
		sa(head_b);
	}
	else if (ft_strcmp(line, "pa") == 0)
		push(head_b, head_a);
	else if (ft_strcmp(line, "pb") == 0)
		push(head_a, head_b);
	else
	{
		ft_printf("Error\n");
		exit(0);
	}
}

int		ft_perexod(t_s *s)
{
	char *line;

	s->l = 0;
	while (get_next_line(0, &line))
	{
		if (ft_strncmp(line, "r", 1) == 0)
			ft_help(line, &s->a, &s->b);
		else if (ft_strncmp(line, "s", 1) == 0 || ft_strncmp(line, "p", 1) == 0)
			ft_help_1(line, &s->a, &s->b);
		else
			return (ft_printf("Error\n"));
		s->l++;
		if (s->v)
			s->c ? ft_show_stack_2(s->a, s->b) : \
			ft_show_stack_3(s->a, s->b);
		free(line);
	}
	free(line);
	ft_printf("%8d - operations \n", s->l);
	if (ft_check(&s->a, &s->b))
		ft_printf("\x1B[42m\x1B[30m%s", "          OK          \n\033[0m");
	else
		ft_printf("\x1b[41m\x1b[30m%s\x1B[0m", "          KO          \n");
	return (1);
}

int		ft_validate(t_list **head_a, long int d)
{
	t_list *tmp;

	tmp = *head_a;
	if (d > 2147483647 || d < -2147483648)
		return (1);
	while (tmp)
	{
		if (d == *(int *)(*head_a)->content)
			return (1);
		tmp = tmp->next;
	}
	return (0);
}
