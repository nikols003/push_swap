/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   help1.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdubina <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/10 17:21:49 by mdubina           #+#    #+#             */
/*   Updated: 2017/02/10 17:27:59 by mdubina          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	ft_push_b_while_1(t_s *s, int *k, int *j)
{
	int i;
	int z;
	int avarage;

	z = ft_lst_count(s->a) - s->count;
	avarage = ft_avarage(s);
	i = 0;
	while (i < z && ft_check_nb(&s->a, avarage))
	{
		if (*(int *)s->a->content < avarage)
		{
			(*k)++;
			ft_transformer("pb", s);
		}
		else
		{
			ft_transformer("ra", s);
			(*j)++;
		}
		i++;
	}
}

int		ft_push_b_while(t_s *s)
{
	int i;
	int j;
	int k;

	j = 0;
	k = 0;
	ft_push_b_while_1(s, &k, &j);
	i = 0;
	while (i < j && s->count)
	{
		if (ft_lst_count(s->b) > 1 && ft_lst_count(s->a) > 1)
		{
			if (*(int *)s->a->content > *(int *)s->a->next->content \
				&& *(int *)s->b->content < \
				*(int *)s->b->next->content)
				ft_transformer("ss", s);
		}
		ft_transformer("rra", s);
		i++;
	}
	return (k);
}

void	ft_push_a_while_1(t_s *s, int *k, int *j)
{
	int i;
	int z;
	int avarage;

	i = 0;
	avarage = ft_avarage_2(s, *k);
	z = *k;
	while (i < z && ft_check_nb_2(&s->b, avarage))
	{
		if (*(int *)s->b->content >= avarage)
		{
			(*k)--;
			ft_transformer("pa", s);
		}
		else
		{
			ft_transformer("rb", s);
			(*j)++;
		}
		i++;
	}
}

int		ft_push_a_while(t_s *s, int k)
{
	int i;
	int j;

	j = 0;
	ft_push_a_while_1(s, &k, &j);
	i = 0;
	if (ft_lst_count(s->b) != k)
	{
		while (i < j)
		{
			if (ft_lst_count(s->b) > 1 && ft_lst_count(s->a) > 1)
			{
				if (*(int *)s->a->content > *(int *)s->a->next->content && \
						*(int *)s->b->content < *(int *)s->b->next->content)
					ft_transformer("ss", s);
			}
			ft_transformer("rrb", s);
			i++;
		}
	}
	return (k);
}

int		ft_push_a(t_s *s, int k)
{
	while (k)
	{
		if (ft_lst_count(s->b) > 1 && ft_lst_count(s->a) > 1)
		{
			if (*(int *)s->a->content > *(int *)s->a->next->content \
					&& *(int *)s->b->content < \
				*(int *)s->b->next->content)
				ft_transformer("ss", s);
		}
		if (*(int *)s->a->content > *(int *)s->a->next->content && \
				ft_lst_count(s->a) > 1)
			ft_transformer("sa", s);
		ft_transformer("pa", s);
		k--;
	}
	return (k);
}
