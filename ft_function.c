/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_function.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdubina <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/10 17:14:21 by mdubina           #+#    #+#             */
/*   Updated: 2017/02/10 18:18:52 by mdubina          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	ft_transformer_1(char *line, t_s *s)
{
	if (ft_strcmp(line, "ra") == 0)
		rotate(&s->a);
	if (ft_strcmp(line, "rb") == 0)
		rotate(&s->b);
	if (ft_strcmp(line, "rr") == 0)
	{
		rotate(&s->a);
		rotate(&s->b);
	}
	if (ft_strcmp(line, "rra") == 0)
		r_rotate(&s->a);
	if (ft_strcmp(line, "rrb") == 0)
		r_rotate(&s->b);
	if (ft_strcmp(line, "rrr") == 0)
	{
		r_rotate(&s->a);
		r_rotate(&s->b);
	}
}

void	ft_transformer(char *line, t_s *s)
{
	s->l++;
	ft_printf("%s\n", line);
	if (ft_strncmp(line, "r", 1) == 0)
		ft_transformer_1(line, s);
	if (ft_strcmp(line, "sa") == 0)
		sa(&s->a);
	if (ft_strcmp(line, "sb") == 0)
		sa(&s->b);
	if (ft_strcmp(line, "ss") == 0)
	{
		sa(&s->a);
		sa(&s->b);
	}
	if (ft_strcmp(line, "pa") == 0)
		push(&s->b, &s->a);
	if (ft_strcmp(line, "pb") == 0)
		push(&s->a, &s->b);
}

int		ft_check_nb(t_list **head_a, int nb)
{
	t_list *tmp;

	tmp = *head_a;
	while (tmp)
	{
		if (*(int *)tmp->content < nb)
			return (1);
		tmp = tmp->next;
	}
	return (0);
}

int		ft_check_nb_2(t_list **head_b, int nb)
{
	t_list *tmp;

	tmp = *head_b;
	while (tmp)
	{
		if (*(int *)tmp->content > nb)
			return (1);
		tmp = tmp->next;
	}
	return (0);
}

int		ft_check_list(t_list **head_a)
{
	t_list *tmp;

	if ((*head_a)->next == NULL)
		return (1);
	tmp = *head_a;
	while (tmp->next)
	{
		if (*((int *)tmp->content) > *((int *)tmp->next->content))
			return (0);
		tmp = tmp->next;
	}
	return (1);
}
